
**1. Что такое git bash?**
   
Git Bash - это командная оболочка, которая предоставляет инструменты командной строки Linux в среде Windows. Она включает в себя утилиты для работы с Git, такие как git, ssh, scp, bash, grep, awk и т.д. Git Bash позволяет использовать эти утилиты в командной строке Windows без необходимости установки Linux на компьютер. Это удобно для разработчиков, которые хотят использовать Git и другие инструменты командной строки на Windows-компьютерах.
___
**2. Что делает команда git pull? Чем она отличается от git push?**
   
Команда `git pull` выполняет обновление локальной ветки репозитория с изменениями, сделанными в удаленном репозитории. Это происходит таким образом, что бы изменения, сделанные удаленно, были загружены и внедрены в локальную ветку репозитория.

Отличие этой команды от команды `git push` заключается в том, что `git push` выполняет обратную операцию. Она отправляет изменения, сделанные локально, в удаленный репозиторий. Таким образом, команда `git push` используется для обновления удаленного репозитория, а `git pull` - для обновления локальной копии репозитория.
___
**3. Что такое merge request?**
   
Merge request (MR) - это запрос на объединение изменений веток кода в Git-репозитории. Когда разработчик работает над кодом в отдельной ветке, он может создать MR, чтобы запросить проверку и объединение его ветки с основной веткой кода. 

MR содержит информацию о том, что было изменено, какие файлы были добавлены или удалены, и что нужно сделать для работы проекта с новыми изменениями. Код-ревьюеры и другие участники проекта могут оставлять комментарии и оценки в MR, а затем разработчик может внести необходимые изменения перед тем, как объединить свои изменения с основной веткой. 

Это важный шаг в рабочем процессе разработки, который помогает не только поддерживать кодовую базу на чистоте, но и координировать работу нескольких разработчиков в одном проекте.
___
**4. Чем между собой отличаются команды git status и git log?**
   
Команда `git status` показывает текущее состояние вашего рабочего каталога. Она показывает, какие файлы изменены, но еще не добавлены для последующего коммита, какие файлы были добавлены, но еще не зафиксированы в коммите, и какие файлы не отслеживаются Git'ом.

Команда `git log`, с другой стороны, показывает историю коммитов в вашем репозитории. Она показывает информацию о каждом коммите, такую как хеш коммита, автор, дата и время коммита, а также сообщение коммита.

Таким образом, команда `git status` показывает текущее состояние вашей рабочей копии репозитория, а команда `git log` показывает историю коммитов в вашем репозитории. Эти команды могут использоваться вместе для отслеживания изменений в вашем проекте.
___
**5. Что такое submodule? С помощью какой команды можно добавлять сабмодули в свой репозиторий?**
   
Submodule (сабмодуль) - это ссылка на другой репозиторий Git, который добавляется внутрь основного репозитория. Это позволяет включать код из других проектов в свой код и управлять ими как отдельными сущностями.

Для добавления сабмодуля в свой репозиторий, используется команда "git submodule add". Например, чтобы добавить сабмодуль с адресом https://github.com/user/repo, нужно выполнить следующую команду в терминале:

    git submodule add https://github.com/user/repo path/to/submodule

Здесь "path/to/submodule" - это путь к директории, где будет храниться субмодуль в основном репозитории.